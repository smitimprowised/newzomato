const csv = require('csv-parser');
var fs = require("fs");
var raw_data = fs.readFileSync("zomato.csv","UTF-8");

function CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    //if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

var lines=raw_data.split("\n");

  var result = [];

  var headers=lines[0].split(",");

  // var currentline = []

  for(var i=1;i<lines.length;i++){

	  var obj = {};
	  //if (String(lines[i])!=='null'){
	  var currentline = CSVtoArray(String(lines[i])); 
   
	  for(var j=0;j<headers.length;j++){
	  	//if (currentline !== 'null')
		  obj[headers[j]] = currentline[j];
	  }
   
	  result.push(obj);
    
  } 
var counter =1;
result.forEach((value) => {

    if(value.City===process.argv[2] && counter <=20){
        console.log("Name:"+value.Restaurant_Name+" ,Address:"+value.Address+" ,Cuisines:"+value.Cuisines+"\n");
        counter++;
    }
  }) 
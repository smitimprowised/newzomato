/* const csv = require('csv-parser');
const fs = require('fs');
let value = [];
fs.createReadStream('zomato.csv')
  .pipe(csv())
  .on('data', function(data) {value.push(data)})
  .on('end',()=> {
  	//module.exports = value;
  	console.log(value)
  });
  
  */


// var columns=[];
// var fs = require("fs");
// fs.readFile("zomato.csv","UTF-8",function(err,contents){

// 	if(err) { throw err;}
// 	else {
// 		var rows = contents.split("\n");
// 		rows.forEach((thisRow) =>{

// 			columns.append(thisRow.split(","));

// 		})
// 		console.log(columns)
// 	}
// })
/*
var fs = require("fs");
var raw_data = fs.readFileSync("zomato.csv","UTF-8");


var data = raw_data.split("\n").map(function(line) { return line.split(','); });

console.log(data[1][1]);
*/

const csv = require('csv-parser');
var fs = require("fs");
var raw_data = fs.readFileSync("zomato.csv","UTF-8");

function CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    //if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

var lines=raw_data.split("\n");

  var result = [];

  var headers=lines[0].split(",");

  // var currentline = []

  for(var i=1;i<lines.length;i++){

	  var obj = {};
	  //if (String(lines[i])!=='null'){
	  var currentline = CSVtoArray(String(lines[i])); 
   
	  for(var j=0;j<headers.length;j++){
	  	//if (currentline !== 'null')
		  obj[headers[j]] = currentline[j];
	  }
   
	  result.push(obj);
    
  } 

  var copyAverage_Cost_for_two = [];

  result.forEach(value => copyAverage_Cost_for_two.push(value.Average_Cost_for_two))
  

  var maxAverage_Cost_for_two = Math.max(... copyAverage_Cost_for_two)
  var minAverage_Cost_for_two = Math.min(... copyAverage_Cost_for_two)
  //Indian Rupees(Rs.)
  //console.log(maxAverage_Cost_for_two)
   result.forEach((value) => {

  	if(value.Average_Cost_for_two==maxAverage_Cost_for_two && value.Currency=='Indian Rupees(Rs.)'){
        console.log("Maximum Average_Cost_for_two:\n");
        console.log("Name:"+value.Restaurant_Name+" ,City:"+value.City+" ,Cuisines:"+value.Cuisines+", Has_Table_booking:"+value.Has_Table_booking+", Ratings:"+value.Aggregate_rating+", Average_Cost_for_two :"+value.Average_Cost_for_two+"\n");
  	}
    if(value.Average_Cost_for_two==minAverage_Cost_for_two && value.Currency=='Indian Rupees(Rs.)'){
        console.log("Minimum Average_Cost_for_two:\n");
        console.log("Name:"+value.Restaurant_Name+" ,City:"+value.City+" ,Cuisines:"+value.Cuisines+", Has_Table_booking:"+value.Has_Table_booking+", Ratings:"+value.Aggregate_rating+", Average_Cost_for_two :"+value.Average_Cost_for_two+"\n");
    }
  }) 